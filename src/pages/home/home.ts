import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public slides: any = [];
  public isFlipped = true;

  constructor(public navCtrl: NavController) {
    this.slides = [
      {
        title: "Bem Vindo à Viagem <br> do Grêmio de 2019!!",
        subtitle: false,
        description: "Aqui você irá encontrar informações sobre cada dia do passeio e os horários dos eventos.",
        image: "../../assets/imgs/traveler.svg",
        events: [
          {
            event: ""
          }
        ]
      },
      {
        title: "Saída de Hortolândia!",
        subtitle: "20/07",
        description: "Nos encotraremos <b>IATec às 21h</b> para realizamors um culto especial antes de iniciarmos a viagem.",
        image: "../../assets/imgs/globe.svg",
        events: [
          {
            event: "21h Encontro no Auditório - IATec"
          },
          {
            event: "22h Saída de Hortolândia - SP"
          }
        ]
      },
      {
        title: "Chegada no Catre",
        subtitle: "21/07",
        description: "Chegaremos no Catre em <b>Santa Catarina por volta das 11h</b>. Lá, estará nos esperando um belo almoço preaparo com todo carinho pela equipq do Catre.",
        image: "../../assets/imgs/people_on_beach.svg",
        events: [
          { event: "11h Chegada em Gov. Celso Ramos" },
          { event: "12h Almoço" },
          { event: "18h Jantar" },
          { event: "20h Culto Auditório" },
        ]
      },
      {
        title: "Passeio de Escuna",
        subtitle: "22/07",
        description: "Para aqueles que optaram pelo <b>passeio de escuna, hoje é o dia!</b>. O passeio será realizado no período da manhã. No período da tarde teremos um passeio para conhecer Florianópolis.",
        image: "../../assets/imgs/boat.svg",
        events: [
          { event: "8h Café da manhã" },
          { event: "10h Saída para Passeio de Escuna" },
          { event: "12h Retorno Para o Catre" },
          { event: "13h Almoço" },
          { event: "14h Saída para Florianópilis" },
          { event: "22h Retorno para o Catre" },
        ]
      },
      {
        title: "Passeio Beto Carrero",
        subtitle: "23/07",
        description: "Hoje é o grande dia do <b>passeio no parque Beto Carrero!</b> Considerado um dos melhores parques do Brasil, o mesmo oferece diversas atrações. O passeio ocorrerá durante a manhã e a tarde.",
        image: "../../assets/imgs/beto_carrero.png",
      },
      {
        title: "Acabou a aventura...",
        subtitle: "24/07",
        description: "Agradecemos a todos vocês que participaram da viagem. <b>Acabou, mas ainda não é o final...</b> Fique sempre ligado para as novidades da viagem do próximo ano :)",
        image: "../../assets/imgs/vacation_end.svg",
      }
    ];
  }
}
